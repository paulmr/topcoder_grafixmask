#include <stdio.h>

#include "mask.h"
#include "search.h"
#include "stack.h"

/* puts an array of neighbours (which have not already been seen, i.e. have 0 in
 * mask) in *n which should have at least enough room for MAX_NEIGHBOURS. Return
 * value will be the number of neighbours found.
 */

int search_get_neighbours(mask_t mask, int p, int *n) {
	int count = 0, x = p2x(p), y = p2y(p);

	if (x > 0) {
		n[count++] = xy2p(x - 1, y);
	}

	if (x < MASK_WIDTH - 1) {
		n[count++] = xy2p(x + 1, y);
	}

	if (y > 0) {
		n[count++] = xy2p(x, y - 1);
	}

	if (y < MASK_HEIGHT - 1) {
		n[count++] = xy2p(x, y + 1);
	}

	return count;
}

/*
 * starting from position p, will traverse the area counting each square that is
 * the same value as p; seen will be used to mark already seen positions
 */

/* search for contiguous blocks of val, returns the area of that block */
int search(int p, mask_t field, mask_t seen, int val) {
	int neighbours[MAX_NEIGHBOURS], num, area = 0;
	stk_t stk = stk_create();
	if (stk == NULL) {
		fprintf(stderr, "Could not allocate stack...");
		return 0;
	}
	
	/* push the first position */
	if (!stk_push(stk, p)) {
		fprintf(stderr, "Could not push to stack (no mem?)...");
		stk_free(stk);
		return 0;
	}

	/* now start the search loop -- continuing for as long as there are still
	 * items on the stack*/
	while (stk_len(stk) > 0) {
		p = stk_pop(stk);
		// does this pos match the value we're searching for? if not, we have
		// reached a dead-end
		if (field[p] != val) {
			continue;
		}
		/* include this in the area count */
		area += 1;
		/* mark as seen */
		seen[p] = 1;
		field[p] = 3;
		/* get the neighbours for this item, and see if they belong on the stack
		 * (i.e. if they need to be checked)
		 */
		num = search_get_neighbours(field, p, neighbours);
		while(--num >= 0) {
			// have we already seen this? if not, add it
			if (seen[neighbours[num]] != 1) {
				if (!stk_push(stk, neighbours[num])) {
					fprintf(stderr, "Could not push to stack (no mem?)...");
					stk_free(stk);
					return 0;
				}
			}
		}
	}
	return area;
}

/* comparison function used by qsort() */
int cmp(void *a, void *b) {
	int *ia = (int *) a;
	int *ib = (int *) b;

	return (*ia > *ib) - (*ia < *ib);
}

/* put an array of areas into arr, on entry for each of the found contiguous
 * blocks, sorted from smallest to largest; return the size of the array */
int *search_find_all(int *sz, mask_t mask, mask_t seen) {
	int i;
	int *ret;
	stk_t stk;

	stk = stk_create();
	if (stk == NULL) {
		fprintf(stderr, "Could not allocate stack\n");
		return 0;
	}

	/* search each cell in the mask, and once we find one that is not filled in,
	 * grab the entire contiguous block */
	for (i = 0; i < MASK_AREA; i++) {
		if (seen[i] != 1 && mask[i] == 0) {
			stk_push(stk, search(i, mask, seen, 0));
		}
	}
	ret = stk_to_array(stk, sz);
	qsort(ret, *sz, sizeof(int), cmp);
	stk_free(stk);
	return ret;
}
