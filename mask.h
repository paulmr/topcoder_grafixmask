#define MASK_HEIGHT 400
#define MASK_WIDTH  600

/* #define MASK_HEIGHT 10 */
/* #define MASK_WIDTH  10 */

typedef short mask_cell_t;
typedef mask_cell_t* mask_t;

#define FILL_MARKER 1

#define MASK_AREA (MASK_HEIGHT * MASK_WIDTH)

/* convert x,y coordinates into a pixel number */
#define xy2p(x, y) (((y) * MASK_WIDTH) + (x))
#define p2x(p)     ((p) % MASK_WIDTH)
#define p2y(p)     ((p) / MASK_WIDTH)

mask_cell_t *mask_create(void);
void mask_free(mask_t mask);

void mask_fill_rect(mask_cell_t *mask, int start_x, int start_y, int end_x,
	int end_y);
/* mostly to be used for debugging */
void mask_print(mask_cell_t *mask);
