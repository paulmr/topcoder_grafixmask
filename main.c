#include <stdio.h>
#include <stdlib.h>
#include "mask.h"
#include "ui.h"
#include "search.h"
#include "stack.h"

int main(int argc, char **argv) {
	int x1, y1, x2, y2, ret, i;
	int neighbours[MAX_NEIGHBOURS];
	int *results;

	stk_t stk;

	mask_t mask, seen;

	mask = mask_create();
	seen = mask_create();

	ui_add_array(argv + 1, argc - 1, mask);

	results = search_find_all(&ret, mask, seen);
	printf("{");
	for (i = 0; i < ret; i++) {
		printf("%s %d", i > 0 ? "," : "", results[i]);
	}
	printf(" }\n");

	mask_free(mask);
	mask_free(seen);
}
