OBJS=mask.o ui.o search.o stack.o main.o

C_OPTS=-g
CC=gcc
EXECUTABLE=grafixMask

test: $(EXECUTABLE)
	./RUNTESTS.sh

$(EXECUTABLE): $(OBJS)
	$(CC) -o $@ $(OBJS)

%.o: %.c
	$(CC) $(C_OPTS) -c $<

clean:
	rm -f $(OBJS) $(EXECUTABLE)
