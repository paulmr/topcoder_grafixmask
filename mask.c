/*
 * this file defines the mask object, which is a global object
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mask.h"

/* create, and return a pointer to, a new mask */
mask_t mask_create(void) {
	mask_t mask;

	if ((mask = calloc(MASK_AREA, sizeof(mask_cell_t)))) {
		bzero(mask, sizeof(mask_cell_t) * MASK_AREA);
	}

	return mask;
}

void mask_free(mask_t mask) {
	free(mask);
}

/* for debugging */
void mask_print(mask_t mask) {
	int p;

	for (p = 0; p < MASK_AREA; p++) {
		  printf("%c%d", (p % MASK_WIDTH == 0) ? '\n' : ' ', mask[p]);
	}
	printf("\n");
}

/*
 * assumes x1,y1 < x2,y2
 */
void mask_fill_rect(mask_t mask, int start_x, int start_y, int end_x,
		int end_y) {
	int x, y;

	for (y = start_y; y <= end_y; y++) {
		for (x = start_x; x <= end_x; x++) {
			mask[xy2p(x, y)] = FILL_MARKER;
		}
	}
}
