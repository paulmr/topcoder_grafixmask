/* how much space will be allocated when first generate, and when it needs to
 * grow this much space will be allocated each time */
#define STK_INIT_SIZE (size_t) 10

typedef int stk_base_t;

typedef struct {
	int sp;
	int size;
	stk_base_t *data;
} *stk_t;

stk_t stk_create();
void stk_free(stk_t stk);
int stk_push(stk_t stk, stk_base_t val);
int stk_pop(stk_t stk);
int stk_len(stk_t stk);
stk_base_t *stk_to_array(stk_t stk, int *sz);
