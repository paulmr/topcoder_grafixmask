#include <stdio.h>
#include "mask.h"

/* converts a string to rectangle co-ordinates, which will be written into the
 * co-ord args provided; returns 1 on success, else 0 */
int ui_str_to_rect(char *str, int *x1, int *y1, int *x2, int *y2) {
	int ret;
	
	ret = sscanf(str, "%d %d %d %d", y1, x1, y2, x2);
	return (ret == 4);
}

/* adds an array of strings */
int ui_add_array(char **str, int count, mask_t mask) {
	int i, x1, y1, x2, y2;

	for (i = 0; i < count; i++) {
		ui_str_to_rect(str[i], &x1, &y1, &x2, &y2);
		mask_fill_rect(mask, x1, y1, x2, y2);
	}
}
