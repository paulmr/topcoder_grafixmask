/* a very simple stack-of-integers implementation */

#include <stdlib.h>
#include <stdio.h>

#include "stack.h"

stk_t stk_create() {
	stk_t stk;
	/* first allocate the memory for the stack structure */
	stk = malloc(sizeof(stk_t));
	if (stk == NULL) {
		return NULL;
	}
	stk->data = calloc(STK_INIT_SIZE, sizeof(stk_base_t));
	if (stk->data == NULL) {
		free(stk);
		return NULL;
	}
	stk->size = STK_INIT_SIZE;
	stk->sp = 0;
}

void stk_free(stk_t stk) {
	free(stk->data);
	free(stk);
}

/* reallocte memory to make the stack the given size, returns true or false
 * depending on whether it succeeded or not */
int stk_resize(stk_t stk, int size) {
	stk_base_t *newmem;
	newmem = realloc(stk->data, size * sizeof(stk_base_t));
	if (newmem != NULL) {
		/* save the new size */
		stk->size = size;
		stk->data = newmem;
	}

	return (newmem != NULL);
}

int stk_push(stk_t stk, stk_base_t val) {
	if ((stk->sp >= stk->size) &&
			!stk_resize(stk, stk->size + STK_INIT_SIZE)) {
		return 0;
	}
	stk->data[stk->sp++] = val;
	return 1;
}

int stk_pop(stk_t stk) {
	if (stk->sp > 0) {
		return stk->data[--stk->sp];
	} else {
		return -1;
	}
}

int stk_len(stk_t stk) {
	return stk->sp;
}

/* copies the contents of the stack to an array and returns it (or NULL if there
 * was an error; sz contains the amount of items returned */
stk_base_t *stk_to_array(stk_t stk, int *sz) {
	int i;
	int *ret;

	*sz = 0;
	if ((ret = calloc(stk_len(stk), sizeof(stk_base_t))) == NULL) {
		return NULL;
	}

	for (i = 0; i < stk_len(stk); i++) {
		ret[i] = stk->data[i];
		*sz += 1;
	}
	return ret;
}
