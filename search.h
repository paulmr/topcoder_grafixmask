/* four, plus the terminator (-1) */
#define MAX_NEIGHBOURS 4

int search(int p, mask_t field, mask_t seen, int val);
int search_get_neighbours(mask_t mask, int p, int *n);
int *search_find_all(int *sz, mask_t mask, mask_t seen);
